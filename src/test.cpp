#include "Ball.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

using namespace std;

const int WIDTH = 1280;
const int HEIGHT = 720;


void initBalls(Ball * balls, int n) {
    
    balls[0].setPX(200);
    balls[0].setPY(HEIGHT/2);
    balls[0].setCouleur({210,210,210});
    //balls[0].setVX(600);
    //balls[0].setVY(150);

    int x = 500;
    
    balls[1].setPX(700);
    balls[1].setPY(HEIGHT/2);
    balls[1].setVX(-200);

    for (int i = 2; i< n; i++) {
        balls[i].setPX(x+i);
        balls[i].setPY(HEIGHT/2 + i);
    }
}


int main(int argc, char* argv[]) {

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

     // Initialisation de SDL_Image
    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    SDL_Window * window; // Création de la fenêtre
    SDL_Renderer * renderer; // Création du moteur de rendu

    // Creation de la fenetre
    window = SDL_CreateWindow("Billard", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; 
        SDL_Quit(); 
        exit(1);
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);


    SDL_Surface* surfBackground = IMG_Load("./assets/images/billard.jpg");
    if(!surfBackground)
    {
        printf("Erreur de chargement de l'image : %s",SDL_GetError());
        return -1;
    }

    SDL_Texture* background = SDL_CreateTextureFromSurface(renderer,surfBackground);

    SDL_Rect backgroundRect = {0,0,1280,720};

    //int elapsedTime;
    
    auto t1 = std::chrono::system_clock::now();
    auto t2 = std::chrono::system_clock::now();
    float ET;


    //=======Initialisation des boules=======//

    Ball * balls;
    balls = new Ball[16];    
    initBalls(balls, 16);

    Ball ball1(200, HEIGHT/2, 0, {210,210,210}); 
    Ball ball2(600, HEIGHT/2, 0, {255,0,210}); 

    bool is_running = true;
    int xMouse, yMouse;
    while ( is_running )
    {
        SDL_Event event;
        if ( SDL_PollEvent( &event ))
        {
            if ( event.type == SDL_QUIT )
            {
                is_running = false;
            }  

            if (event.type == SDL_MOUSEMOTION)
            {
            SDL_GetMouseState(&xMouse,&yMouse);
            } 

            if (event.type == SDL_MOUSEBUTTONDOWN) 
            {   
                balls[0].setVX(5.0f * (balls[0].getPX() - xMouse));
                balls[0].setVY(5.0f * (balls[0].getPY() - yMouse));
            }
        }

        t2 = std::chrono::system_clock::now();
        std::chrono::duration<float> elapsedTime = t2 - t1;
        t1 = t2;
        float fElapsedTime = elapsedTime.count();
        ET = fElapsedTime;



        SDL_RenderClear (renderer);

        SDL_RenderCopy(renderer, background, NULL, &backgroundRect);

        //SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        //SDL_RenderDrawLine(renderer, balls[0].getPX(), balls[0].getPY(), fabs(5.0f * (balls[0].getPX() - xMouse)), fabs(5.0f * (balls[0].getPY() - yMouse)) );
        

        //balls[0].setPX(xMouse);
        //balls[0].setPY(yMouse);

        //=======Collisions entre les boules=======//

        for (int i = 0; i<16; i++) {
            for (int j = 0; j<16; j++) {
                if (i == j) {
                }
                else {
                    if (balls[i].collisionEntreBoules(balls[j])) {
                    balls[i].collisionMove(balls[j]);
                    balls[i].dynamicCollision(balls[j]);
                    }
                }
            }
        }

        //=======Actualisation et affichage des boules=======//

        for (int i =0; i< 16; i++) {
            balls[i].draw(renderer);
        }

        //balls[0].updateBall(ET);

        for (int i = 0; i<16; i++) 
        {
            balls[i].updateBall(ET);
        }


        SDL_RenderPresent ( renderer ); 
    }

    // Ferme et détruit la fenetre
    SDL_DestroyWindow(window);
    SDL_FreeSurface(surfBackground);

    SDL_Quit();
    return 0;
}