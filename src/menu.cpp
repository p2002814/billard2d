#include "Ball.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include "Wall.h"
#include "math.h"

using namespace std;

const int WIDTH = 1280;
const int HEIGHT = 720;

int main(int argc, char* argv[]) {

// Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

     // Initialisation de SDL_Image
    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    SDL_Window * window; // Création de la fenêtre
    SDL_Renderer * renderer; // Création du moteur de rendu

    // Creation de la fenetre
    window = SDL_CreateWindow("Billard", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; 
        SDL_Quit(); 
        exit(1);
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);

//Surface fond écran

    SDL_Surface* surfBackground = IMG_Load("./assets/images/billard90autiliser.png");
    if(!surfBackground)
    {
        printf("Erreur de chargement de l'image : %s",SDL_GetError());
        return -1;
    }

    SDL_Texture* background = SDL_CreateTextureFromSurface(renderer,surfBackground);

    SDL_Rect backgroundRect = {0,0,1280,720};

//Surface Nouvelle partie

    SDL_Surface* surfNouvellePartie = IMG_Load("./assets/images/Nouveau.png");
    if(!surfBackground)
    {
        printf("Erreur de chargement de l'image : %s",SDL_GetError());
        return -1;
    }

    SDL_Texture* nouvellepartie = SDL_CreateTextureFromSurface(renderer,surfNouvellePartie);

    SDL_Rect nouvellepartieREC = {0,0,356,33};

//Surface Quitter

    SDL_Surface* surfQuitter = IMG_Load("./assets/images/Quitter.png");
    if(!surfBackground)
    {
        printf("Erreur de chargement de l'image : %s",SDL_GetError());
        return -1;
    }

    SDL_Texture* quitter = SDL_CreateTextureFromSurface(renderer,surfQuitter);

    SDL_Rect QuitterpartieREC = {0,0,189,33};

//Surface Menu

    SDL_Surface* surfMenu = IMG_Load("./assets/images/menu2.png");
    if(!surfBackground)
    {
        printf("Erreur de chargement de l'image : %s",SDL_GetError());
        return -1;
    }

    SDL_Texture* menu = SDL_CreateTextureFromSurface(renderer,surfMenu);

    SDL_Rect MenuREC = {0,0,144,33};



    bool is_running = true;
    while(is_running)
    {
        SDL_Event event;

        if ( SDL_PollEvent( &event ))
        {
            if ( event.type == SDL_QUIT )
            {
                is_running = false;
            }  
        }

        SDL_RenderClear(renderer);

        SDL_RenderCopy(renderer,background, NULL, &backgroundRect); //copy du background

        SDL_RenderCopy(renderer,nouvellepartie, NULL, &nouvellepartieREC);//copy de Nouvelle partie

        SDL_RenderCopy(renderer,menu, NULL, &MenuREC);//copy de Menu

        SDL_RenderCopy(renderer,quitter, NULL, &QuitterpartieREC);//copy de Quitter

        SDL_RenderPresent (renderer); 
    }
    
    
    // Ferme et détruit la fenetre
    SDL_DestroyWindow(window);
    SDL_FreeSurface(surfBackground);
    SDL_FreeSurface(surfNouvellePartie);
    SDL_FreeSurface(surfMenu);
    SDL_FreeSurface(surfQuitter);

    SDL_Quit();
    return EXIT_SUCCESS;
}