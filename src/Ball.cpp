#include "Ball.h"
#include <iostream>
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include <stdio.h>
#include <math.h>
#include <time.h> 

using namespace std;

Ball::Ball(float positionX, float positionY, int identification, Couleur couleurBalle) {
    px = positionX;
    py = positionY;
    vx = 0;
    vy = 0;
    ax = 0;
    ay = 0;
    id = identification;
    couleur = couleurBalle;
}

Ball::Ball() {

}

Ball::~Ball() {

}

void Ball::setPX(float positionX) {
    px = positionX;
}

void Ball::setPY(float positionY) {
    py = positionY;
}

void Ball::setAX(float accelerationX) {
    ax = accelerationX;
}

void Ball::setAY(float accelerationY) {
    ay = accelerationY;
}

void Ball::setVX(float velociteX) {
    vx = velociteX;
}

void Ball::setVY(float velociteY) {
    vy = velociteY;
}

float Ball::getPX() {
    return px;
}

float Ball::getPY() {
    return py;
}

float Ball::getVX() {
    return vx;
}

float Ball::getVY() {
    return vy;
}

void Ball::draw(SDL_Renderer * renderer)
{
    SDL_SetRenderDrawColor (renderer, couleur.r, couleur.g, couleur.b, 255);
    
    int offsetx, offsety, d;

    offsetx = 0;
    offsety = rayon;
    d = rayon -1;

    while (offsety >= offsetx) {

        SDL_RenderDrawLine(renderer, px - offsety, py + offsetx, px + offsety, py + offsetx);
        SDL_RenderDrawLine(renderer, px - offsetx, py + offsety, px + offsetx, py + offsety);
        SDL_RenderDrawLine(renderer, px - offsetx, py - offsety, px + offsetx, py - offsety);
        SDL_RenderDrawLine(renderer, px - offsety, py - offsetx, px + offsety, py - offsetx);

        // SHADOW

        if (d >= 2*offsetx) {
            d -= 2*offsetx + 1;
            offsetx +=1;
        }
        else if (d < 2 * (rayon - offsety)) {
            d += 2 * offsety - 1;
            offsety -= 1;
        }
        else {
            d += 2 * (offsety - offsetx - 1);
            offsety -= 1;
            offsetx += 1;
        }
    }
}

bool Ball::collisionEntreBoules(Ball ball2) {
     return ((px-ball2.px)*(px-ball2.px) + (py-ball2.py) * (py-ball2.py)) <= (rayon + ball2.rayon) * (rayon + ball2.rayon);
}

void Ball::collisionMove(Ball ball2) {

    // // Distance entre les centres des deux balles
    float distance = sqrtf((px - ball2.px) * (px - ball2.px) + (py - ball2.py) * (py - ball2.py));
    float collision = (distance - rayon - ball2.rayon)/2;

    // Deplacement de la balle 1
    px -= collision * (px - ball2.px) / distance;
    py -= collision * (py - ball2.py) / distance;
    
    // Deplacement de la balle 2
    ball2.px += collision * (px - ball2.px) / distance;
    ball2.py += collision * (py - ball2.py) / distance;
}

void Ball::dynamicCollision(Ball ball2) {

    float distance = sqrtf((px - ball2.px) * (px - ball2.px) + (py - ball2.py) * (py - ball2.py));

    // norme
    float nx = (ball2.getPX() - px) / distance;
    float ny = (ball2.getPY() - py) / distance;

    // tangente
    float tx = -ny;
    float ty = nx;

    // tangentes entre les deux centres
    float tan1 = vx * tx + vy * ty;
    float tan2 = ball2.getVX() * tx + ball2.getVY() * ty;

    // norme entre les deux centres
    float norm1 = vx * nx + vy * ny;
    float norm2 = ball2.getVX() * nx + ball2.getVY() * ny;

    // conservation de movement
    float m1 = (norm1 * 0 + 2.0f * 50.0f * norm2) / 100.0f;
    float m2 = (norm2 * 0 + 2.0f * 50.0f * norm1) / 100.0f;

    //cout << nx << endl;

    vx = tx * tan1 + nx * m1;
    vy = ty * tan1 + nx * m1;
    ball2.setVX(tx * tan2 + nx * m2);
    ball2.setVY(ty * tan2 + ny * m2);
}

void Ball::updateBall(float elapsedTime) {

    ax = -vx * 0.8f;
    ay = -vy * 0.8f;
    vx += ax * elapsedTime;
    vy += ay * elapsedTime;
    px += vx * elapsedTime;
    py += vy * elapsedTime;

    if (fabs(vx*vx + vy*vy) < 0.01f) {
        vx = 0;
        vy = 0;
    }
}

void Ball::setCouleur(Couleur couleurBalle) {
    couleur = couleurBalle;
}

