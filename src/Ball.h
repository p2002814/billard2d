#pragma once
#include <iostream>
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>

struct Couleur {
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

class Ball
{
    private:

        float px, py;
        float vx, vy = 0;
        float ax, ay = 0;
        int id;
        int rayon = 15;
        Couleur couleur;
        std::vector<Ball> vecBalls;
        

    public:

        Ball(float positionX, float positionY, int identification, Couleur couleurBalle);

        Ball();

        ~Ball();

        float getPX();
        float getPY();
        float getVX();
        float getVY();
        float getAX();
        float getAY();
        int getID();

        void setPX(float positionX);
        void setPY(float positionY);
        void setVX(float velociteX);
        void setVY(float velociteY);
        void setAX(float accelerationX);
        void setAY(float accelerationY);
        void setCouleur(Couleur couleurBalle);

        void draw(SDL_Renderer * renderer);

        bool collisionEntreBoules(Ball ball2);

        void collisionMove(Ball ball2);
        void dynamicCollision(Ball ball2);

        void updateBall(float elapsedTime);

};